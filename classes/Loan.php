<?php

class Loan
{
    /** @var float */
    protected $loanAmount;

    /** @var float */
    protected $interestRate;

    /** @var string */
    protected $originationDate;

    /** @var array */
    protected $paymentSchedule = [];

    /** @var int */
    protected $interestPeriods;

    /** @var int */
    protected $unitPeriod;

    /**
     * Loan constructor.
     * Call this method on each newly-created object
     *
     * @param $loanAmount
     * @param $interestRate
     * @param $originationDate
     * @param $paymentSchedule
     * @param $interestPeriods
     * @param $unitPeriod
     */
    function __construct($loanAmount, $interestRate, $originationDate, $paymentSchedule, $interestPeriods, $unitPeriod)
    {
        $this->loanAmount = $loanAmount;
        $this->interestRate = $interestRate;
        $this->originationDate = $originationDate;
        $this->paymentSchedule = $paymentSchedule;
        $this->interestPeriods = $interestPeriods;
        $this->unitPeriod = $unitPeriod;
    }

    /**
     * @return float
     */
    public function getInterestRate()
    {
        return $this->interestRate;
    }

    /**
     * @return string
     */
    public function getOriginationDate()
    {
        return $this->originationDate;
    }

    /**
     * @return float
     */
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    /**
     * @return array
     */
    public function getPaymentSchedule()
    {
        return $this->paymentSchedule;
    }

    /**
     * @return int
     */
    public function getInterestPeriods()
    {
        return $this->interestPeriods;
    }

    /**
     * @return int
     */
    public function getUnitPeriod()
    {
        return $this->unitPeriod;
    }

    /**
     * Rendered from APR equation function for a performance because it has made excess of loops.
     *
     * @return float|int
     */
    public function getPaymentAmount()
    {
        $paymentAmount = 0;
        if(isset($this->interestPeriods, $this->unitPeriod) && $this->interestPeriods != 0 && $this->unitPeriod != 0) {
            $paymentAmount = $this->loanAmount * (($this->interestRate / $this->interestPeriods) / (1 - pow((1 + ($this->interestRate / $this->interestPeriods)), -count($this->paymentSchedule))));
        }
        return $paymentAmount;
    }

    /**
     * Rendered from APR equation function for a performance because it has made excess of loops.
     *
     * @return array
     */
    public function getExtraDaysAndWholePeriod()
    {
        $extraDaysAndWholePeriod = array(
            'whole_periods' => [],
            'extra_days' => [],
        );

        for ($x = 1; $x <= count($this->paymentSchedule); $x++) {
            $extraDaysAndWholePeriod['whole_periods'][$x] = $x;
            $extraDaysAndWholePeriod['extra_days'][$x] = (strtotime(date($this->paymentSchedule[$x])) - strtotime($this->originationDate . " + " . $this->unitPeriod * ($x) . " days")) / (86400);

            while ($extraDaysAndWholePeriod['extra_days'][$x] > $this->unitPeriod) {
                $extraDaysAndWholePeriod['extra_days'][$x] = $extraDaysAndWholePeriod['extra_days'][$x] - $this->unitPeriod;
                $extraDaysAndWholePeriod['whole_periods'][$x]++;
            }
            while ($extraDaysAndWholePeriod['extra_days'][$x] < 0) {
                $extraDaysAndWholePeriod['extra_days'][$x] = $extraDaysAndWholePeriod['extra_days'][$x] + $this->unitPeriod;
                $extraDaysAndWholePeriod['whole_periods'][$x]--;
            }
        }
        return $extraDaysAndWholePeriod;
    }

    /**
     * Calculating APR through a goal seek with help of APR equation function.
     * In function make a few increase for aprGuess every loop when condition is not zero.
     * And return a result when APR equation is zero.
     *
     * @return float
     */
    public function getApr()
    {
        $aprGuess = $this->interestRate;
        if(isset($this->interestPeriods, $this->unitPeriod) && $this->interestPeriods != 0 && $this->unitPeriod != 0) {

            $paymentAmount = $this->getPaymentAmount();
            $extraDaysAndWholePeriod = $this->getExtraDaysAndWholePeriod();
            $wholePeriods = $extraDaysAndWholePeriod['whole_periods'];
            $extraDays = $extraDaysAndWholePeriod['extra_days'];
            while (round($this->aprEquation($aprGuess, $paymentAmount, $wholePeriods, $extraDays), 2) != 0) {
                $aprEquation = round($this->aprEquation($aprGuess, $paymentAmount, $wholePeriods, $extraDays), 2);
                $increaseApr = pow(10, -(strlen(intval($this->loanAmount))-1));
                $aprGuess = $aprEquation * ($increaseApr) + $aprGuess;
            }
        }
        return $aprGuess;
    }

    /**
     * It was apr function in test task.
     *
     * @param $aprGuess
     * @param $paymentAmount
     * @param $wholePeriods
     * @param $extraDays
     * @return float|int
     */
    public function aprEquation($aprGuess, $paymentAmount, $wholePeriods, $extraDays)
    {
        $numberOfPayments = count($this->paymentSchedule);
        if(isset($this->interestPeriods, $this->unitPeriod) && $this->interestPeriods != 0 && $this->unitPeriod != 0) {
            $periodApr = $aprGuess / (365 / $this->unitPeriod);
        }

        $calculatedApr = -$this->loanAmount;
        if(isset($this->interestPeriods, $this->unitPeriod, $paymentAmount, $periodApr) && $this->interestPeriods != 0 && $this->unitPeriod != 0) {
            for ($i = 1; $i <= $numberOfPayments; $i++) {
                $calculatedApr += $paymentAmount / ((1 + (($extraDays[$i] / $this->unitPeriod) * $periodApr)) * (pow((1 + $periodApr), $wholePeriods[$i])));
            }
        }
        return $calculatedApr;
    }
}